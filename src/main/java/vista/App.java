package vista;

import java.sql.SQLException;

import controladorbd.PersistenciaCliente;
import modelo.Cliente;

public class App {

	public static void main(String[] args) {
		PersistenciaCliente persistCliente = null;
		try {
			persistCliente = new PersistenciaCliente();
			
			// Ver primer registro
			Cliente cliente = persistCliente.getPrimero();
			System.out.println("Primer cliente: " + cliente);
			
			// Ver último registro
			cliente = persistCliente.getUltimo();
			System.out.println("Ultimo cliente: " + cliente);

			// Nuevo cliente
			cliente = new Cliente("Sergio", "Calle Olmo, 5");
			persistCliente.insertar(cliente);
			System.out.println("Registro insertado!");

			// Modificar ultimo cliente. Nos situamos en el que
			// queremos modificar y le pasamos el objeto con los
			// cambios a realizar.
			cliente = persistCliente.getUltimo();
			cliente.setNombre("Sergi");
			persistCliente.modificar(cliente);
			System.out.println("Registro modificado!");

			// Borrar penúltimo cliente
			// persistCliente.ultimo(); persistCliente.anterior();
			persistCliente.irRegistro(persistCliente.totalRegistros() - 1);
			persistCliente.borrar();
			System.out.println("Registro eliminado!");

			// Mostrar total registros
			System.out.println("Total clientes: " + persistCliente.totalRegistros());
			
			persistCliente.cerrar();
		} catch (SQLException e) {
			System.err.println("Error: " + e.getMessage());
		} 

	}

}
